
import ItemInput from "./ItemInput";
import React, {useState} from "react";
import ItemList from "./ItemList";

const Todo = () => {
    const [todoList, setTodoList] = useState([])

    const addTodo = (todo) => {
        const newTodoList = [...todoList, todo]
        setTodoList(newTodoList)
    }

    return (
        <div>
            <div>Todo List</div>
            <ItemList todoList={todoList}/>
            <ItemInput addTodo={addTodo}/>
        </div>
    );

}
export default Todo;