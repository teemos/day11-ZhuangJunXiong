import {useState} from "react";

const ItemInput = (props) => {
    const {addTodo} = props
    const [text,setText]=useState('');
    const handleInputChange = (event) => {
        const value = event.target.value;
        setText(value);
    };
    const add = () => {
        addTodo(text)
        setText ('')
    }
    return (
        <div>
            <input type="text" value={text} onChange={handleInputChange}/>
            <button onClick={add}>add</button>
        </div>
    )
}
export default ItemInput;