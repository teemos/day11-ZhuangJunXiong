import Item from "./Item";

const ItemList = (props) => {
    const {todoList} = props

    return(
        <div>
            {todoList.map((todo, index) => {
                return <Item todo={todo} key={index}/>
            })}
        </div>
    )
}

export default ItemList;