1.Key learning, skills and experience acquired
● HTML and CSS: This morning, I briefly reviewed HTML and CSS, which are the foundations of front-end development. Although they are very simple and I am familiar with them, I do not remember some of the concepts very much. Although they do not affect subsequent React development, I still need to review them.
● React: The React we learned today is the JS library, which can make it easier for us to write front-end code. I have only used VUE for development before, but these two frameworks are still very different. Personally, I feel that React is relatively complex.
2.Problem / Confusing / Difficulties
● The React component I learned in the afternoon is quite complex in passing values, without the bidirectional binding like Vue. Its values are passed back and forth, making people dizzy. It feels more complex than the backend, which suddenly makes me less interested in the front-end.
3.Other Comments / Suggestion
● I hope the teacher can give a brief demonstration before doing the exercise, otherwise I will be under a lot of pressure to complete it in a limited time without fully understanding it.
# ORID
## O
HTML,CSS and React
## R
bad
## I
The React component I learned in the afternoon is quite complex in passing values, without the bidirectional binding like Vue. Its values are passed back and forth, making people dizzy. It feels more complex than the backend, which suddenly makes me less interested in the front-end.
## D
I will spend more time to learn React.
